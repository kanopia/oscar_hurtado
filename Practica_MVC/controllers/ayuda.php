<?php

class Ayuda extends Controller{
 
// Extiendo a controller donde estoy llamando un objeto view,
// en view tengo una funcion render que recibe una ruta para la vista
    function __construct(){
        parent::__construct();
        $this->view->render('ayuda/index');
    }
}